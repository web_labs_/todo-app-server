import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';

import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { DatabaseModule } from './database/database.module';
import { TodoModule } from './todos/todo.module';

@Module({
  imports: [HttpModule, UsersModule, AuthModule, DatabaseModule, TodoModule],
})
export class AppModule {
}
