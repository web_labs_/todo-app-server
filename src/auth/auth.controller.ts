import { Body, Controller, Post } from '@nestjs/common';
import {
  ApiBody, ApiCreatedResponse, ApiForbiddenResponse, ApiTags,
} from '@nestjs/swagger';

import { AuthService } from './services/auth.service';
import { SignUpRequest } from './dto/signUp/signUp.request.dto';
import { SignUpResponse } from './dto/signUp/signUp.response.dto';
import { SignInRequest } from './dto/signIn/signIn.request.dto';
import { SignInResponse } from './dto/signIn/signIn.response.dto';

@ApiTags('Auth')
@ApiForbiddenResponse({
  description: 'Incorrect email or password',
})
@Controller()
export class AuthController {
  constructor(
    private readonly authService: AuthService,
  ) {
  }

  @ApiBody({
    type: SignUpRequest,
  })
  @ApiCreatedResponse({
    description: 'User was registered!',
    type: SignUpResponse,
  })
  @Post('signUp')
  async signUp(@Body() dto: SignUpRequest): Promise<SignUpResponse> {
    return this.authService.signUp(dto);
  }

  @ApiBody({
    type: SignInRequest,
  })
  @ApiCreatedResponse({
    description: 'User was authenticated!',
    type: SignInResponse,
  })
  @Post('signIn')
  async signIn(@Body() dto: SignInRequest): Promise<SignInResponse> {
    return this.authService.signIn(dto);
  }
}
