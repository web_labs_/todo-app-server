import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';

import { AuthController } from './auth.controller';
import { UserEntity } from '../users/entities/user.entity';
import { AuthService } from './services/auth.service';
import { JwtStrategy } from './strategies/jwt.strategy';
import { CryptService } from './services/crypt.service';

@Module({
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, CryptService],
  imports: [
    TypeOrmModule.forFeature([UserEntity]),
    JwtModule.register({}),
  ],
})
export class AuthModule {
}
