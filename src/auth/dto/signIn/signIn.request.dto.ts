import { ApiProperty } from '@nestjs/swagger';

import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';

export class SignInRequest {
  @ApiProperty({
    example: 'user@gmail.com',
    minLength: 5,
    maxLength: 35,
  })
  @IsNotEmpty({ message: 'Email must be not empty!' })
  @IsString({ message: 'Email must be string!' })
  @IsEmail({ message: 'Email address is invalid!' })
  @Length(5, 35)
  readonly email: string;

  @ApiProperty({
    example: 'pas$ord',
    minLength: 6,
    maxLength: 50,
  })
  @IsNotEmpty({ message: 'Password must be not empty!' })
  @IsString({ message: 'Password must be string' })
  @Length(6, 50)
  readonly password: string;
}