import { ApiProperty } from '@nestjs/swagger';

import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';

export class SignUpRequest {
  @ApiProperty({
    example: 'NickName',
    minLength: 3,
    maxLength: 25,
  })
  @IsNotEmpty({ message: 'Username must be not empty!' })
  @IsString({ message: 'Username must be string' })
  @Length(3, 25)
  readonly username: string;

  @ApiProperty({
    example: 'user@gmail.com',
    minLength: 5,
    maxLength: 35,
  })
  @IsNotEmpty({ message: 'Email must be not empty!' })
  @IsString({ message: 'Email must be string!' })
  @IsEmail({ message: 'Email address is invalid!' })
  @Length(5, 35)
  readonly email: string;

  @ApiProperty({
    example: 'pas$word',
    minLength: 6,
    maxLength: 50,
  })
  @IsNotEmpty({ message: 'Password must be not empty!' })
  @IsString({ message: 'Password must be string' })
  @Length(6, 50)
  readonly password: string;
}