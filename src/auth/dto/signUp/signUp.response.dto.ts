import { ApiResponseProperty } from '@nestjs/swagger';

import { IsNotEmpty, IsString } from 'class-validator';

export class SignUpResponse {
  @ApiResponseProperty()
  @IsNotEmpty()
  @IsString()
  readonly accessToken: string;
}