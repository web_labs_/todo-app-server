import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';

import { CryptService } from './crypt.service';
import { UserEntity } from '../../users/entities/user.entity';
import { SignUpRequest } from '../dto/signUp/signUp.request.dto';
import { SignUpResponse } from '../dto/signUp/signUp.response.dto';
import { SignInRequest } from '../dto/signIn/signIn.request.dto';
import { SignInResponse } from '../dto/signIn/signIn.response.dto';
import { accessTokenDuration, secretString } from '../../config/config';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly usersRepository: Repository<UserEntity>,
    private readonly cryptService: CryptService,
    private readonly jwtService: JwtService,
  ) {
  }

  private async getToken(
    payload: { sub: string, username: string, email: string },
  ): Promise<string> {
    const accessToken = await this.jwtService.signAsync(payload, {
      secret: process.env.SECRET_KEY || secretString,
      expiresIn: accessTokenDuration,
    });

    return accessToken;
  }

  async signUp(dto: SignUpRequest): Promise<SignUpResponse> {
    const user = await this.usersRepository.findOne({
      where: { email: dto.email },
    });

    if (!!user) {
      throw new ForbiddenException(`User with ${dto.email} email address already exists!`);
    }

    const hashPassword = await this.cryptService.hash(dto.password);

    const newUser = await this.usersRepository.save({
      ...dto,
      password: hashPassword,
    });

    const payload = {
      sub: newUser.id,
      username: newUser.username,
      email: newUser.email,
    };

    const accessToken = await this.getToken(payload);

    return { accessToken };
  }

  async signIn(dto: SignInRequest): Promise<SignInResponse> {
    const user = await this.usersRepository.findOne({
      where: { email: dto.email },
    });

    const isPasswordEquals = await this.cryptService.compare(
      dto.password,
      user.password,
    );

    if (!user || !isPasswordEquals) {
      throw new NotFoundException('Incorrect email or password!');
    }

    const payload = {
      sub: user.id,
      username: user.username,
      email: user.email,
    };

    const accessToken = await this.getToken(payload);

    return { accessToken };
  }
}