import { ApiProperty } from '@nestjs/swagger';

import { IsBoolean, IsNotEmpty, IsString, Length } from 'class-validator';

export class CreateTodoDto {
  @ApiProperty({
    example: 'example task',
    minLength: 1,
    maxLength: 25,
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  @Length(1, 25)
  title: string;

  @IsBoolean()
  done: boolean;
}