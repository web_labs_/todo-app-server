import { ApiProperty } from '@nestjs/swagger';

import {
  Column, CreateDateColumn, Entity, JoinColumn, ManyToOne,
  PrimaryGeneratedColumn, UpdateDateColumn,
} from 'typeorm';

import { UserEntity } from '../../users/entities/user.entity';

@Entity({ name: 'todos' })
export class TodosEntity {
  @ApiProperty({
    description: 'UUID of todo.',
    required: false,
  })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty({
    example: 'example task',
    minLength: 1,
    maxLength: 25,
    required: true,
  })
  @Column({
    type: 'varchar',
    length: 25,
  })
  title: string;

  @Column({
    type: 'boolean',
  })
  done: boolean;

  @ManyToOne(
    type => UserEntity,
    (user) => user.todos,
    {
      onDelete: 'CASCADE',
      eager: true,
    },
  )
  @JoinColumn()
  user: UserEntity;

  @ApiProperty({
    description: 'Id of owner. Get from JWT token.',
  })
  @Column()
  userId: string;

  @ApiProperty({
    required: false,
  })
  @CreateDateColumn()
  createdAt: string;

  @ApiProperty({
    required: false,
  })
  @UpdateDateColumn()
  updatedAt: string;
}