import { Controller, Get, UseGuards } from '@nestjs/common';
import { Crud, CrudAuth, CrudController } from '@nestjsx/crud';
import {
  ApiBearerAuth, ApiCreatedResponse, ApiForbiddenResponse, ApiTags, ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import { TodosEntity } from './entities/todos.entity';
import { CreateTodoDto } from './dto/createTodo.dto';
import { UpdateTodoDto } from './dto/updateTodo.dto';
import { TodoService } from './todo.service';
import { UserEntity } from '../users/entities/user.entity';
import { JwtAuthGuard } from '../auth/guards/jwt.auth.guard';

@ApiTags('Todos')
@ApiBearerAuth('JWT')
@ApiUnauthorizedResponse({
  description: 'Unauthorized',
})
@ApiForbiddenResponse({
  description: 'Forbidden',
})
@Crud({
  model: {
    type: TodosEntity,
  },
  params: {
    id: {
      field: 'id',
      type: 'string',
      primary: true,
    },
  },
  query: {
    sort: [
      {
        field: 'title',
        order: 'ASC',
      },
      {
        field: 'id',
        order: 'ASC',
      },
    ],
    join: {
      content: {
        eager: true,
      },
    },
  },
  dto: {
    create: CreateTodoDto,
    update: UpdateTodoDto,
  },
})
@CrudAuth({
  property: 'user',
  filter: (user: UserEntity) => ({
    userId: user.id,
  }),
  persist: (user: UserEntity) => ({
    userId: user.id,
  }),
})
@Controller('todos')
@UseGuards(JwtAuthGuard)
export class TodoController implements CrudController<TodosEntity> {
  constructor(public service: TodoService) {
  }
}
