import { Injectable, Scope } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';

import { TodosEntity } from './entities/todos.entity';

@Injectable({ scope: Scope.REQUEST })
export class TodoService extends TypeOrmCrudService<TodosEntity> {
  constructor(
    @InjectRepository(TodosEntity)
    private readonly todoRepository: Repository<TodosEntity>,
  ) {
    super(todoRepository);
  }
}
